// Bootstrap grid system components (row, col)
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

	console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row className='d-flex justify-content-center my-5 pt-5'>
            <Col md={9}>
                <h1>{title}</h1>
                <p>{content}</p>
                <Link className="btn btn-success btn-block"  to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}