import { Row, Col, Card } from 'react-bootstrap';
import givibox from '../images/givibox.webp';
import ls2helmet from '../images/ls2helmet.jpg';
import pirellitires from '../images/pirellitires.jpg';
import { Link } from 'react-router-dom';

export default function Highlights() {
  return (
    <Row className="d-flex justify-content-center align-items-center">
      <Col xs={12} md={3}>
        <Card className="cardHighlight  bg-light">
          
          <Card.Body className="d-flex flex-column align-items-center">
		  	<Card.Img className='w-75' variant="top" src={givibox} />
            <Card.Title>
              <h2>Givi SRA3112 Aluminum Top Case Rack</h2>
            </Card.Title>
            <Card.Text>
              The Givi SRA3112 Aluminum Top Case Rack provides a sturdy and secure mounting point for a top case on your motorcycle.
            </Card.Text>
			<Link className="btn btn-success mx-2 rounded-3 w-50" to={`/products`}>
              More Products
            </Link>
          </Card.Body>
		  
        </Card>
      </Col>
      <Col xs={12} md={3}>
        <Card className="cardHighlight  bg-light">
          
          <Card.Body className="d-flex flex-column align-items-center">
		  	<Card.Img className='w-75' variant="top" src={ls2helmet} />
            <Card.Title>
              <h2>LS2 Stream Snake Full-Face Helmet</h2>
            </Card.Title>
            <Card.Text>
              The LS2 Stream Snake Full-Face Helmet offers a lightweight and aerodynamic design, with a durable polycarbonate shell and a comfortable, removable inner liner.
            </Card.Text>
			<Link className="btn btn-success mx-2 rounded-3 w-50" to={`/products`}>
              More Products
            </Link>
          </Card.Body>
        </Card>
      </Col>
      <Col xs={12} md={3}>
        <Card className="cardHighlight  bg-light">
          
          <Card.Body className="d-flex flex-column align-items-center">
		  	<Card.Img className='w-75' variant="top" src={pirellitires} />
            <Card.Title>
              <h2>Pirelli Angel GT II Motorcycle Tires</h2>
            </Card.Title>
            <Card.Text>
              The Pirelli Angel GT II Motorcycle Tires offer superior grip and handling in both wet and dry conditions, These tires also provide excellent mileage and durability 
            </Card.Text>
			<Link className="btn btn-success mx-2 rounded-3 w-50" to={`/products`}>
              More Products
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}

















// // Bootstrap grid system components
// import { Row, Col, Card } from 'react-bootstrap';

// export default function Highlights() {
// 	return (
// 		<Row className="mt-3 mb-3 d-flex justify-content-center">
// 			<Col  xs={12} md={3}>
// 				<Card className="cardHighlight p-3 bg-light">
// 			      <Card.Body>
// 			        <Card.Title>
// 			        	<h2>Givi SRA3112 Aluminum Top Case Rack</h2>
// 			        </Card.Title>
// 			        <Card.Text>
// 					The Givi SRA3112 Aluminum Top Case Rack provides a sturdy and secure mounting point for a top case on your motorcycle. Made from lightweight aluminum and designed specifically for your bike, this rack is a must-have accessory for long-distance riders.
// 			        </Card.Text>
// 			      </Card.Body>
// 			    </Card>
// 			</Col>
// 			<Col xs={12} md={3}>
// 				<Card className="cardHighlight p-3 bg-light">
// 			      <Card.Body>
// 			        <Card.Title>
// 			        	<h2>LS2 Stream Snake Full-Face Helmet</h2>
// 			        </Card.Title>
// 			        <Card.Text>
// 					The LS2 Stream Snake Full-Face Helmet offers a lightweight and aerodynamic design, with a durable polycarbonate shell and a comfortable, removable inner liner. This helmet also features a unique snake-inspired graphic for a bold and stylish look.
// 			        </Card.Text>
// 			      </Card.Body>
// 			    </Card>
// 			</Col>
// 			<Col xs={12} md={3}>
// 				<Card className="cardHighlight p-3 bg-light">
// 			      <Card.Body>
// 			        <Card.Title>
// 			        	<h2>Pirelli Angel GT II Motorcycle Tires</h2>
// 			        </Card.Title>
// 			        <Card.Text>
// 					The Pirelli Angel GT II Motorcycle Tires offer superior grip and handling in both wet and dry conditions, with a dual-compound tread and innovative design. These tires also provide excellent mileage and durability for long-distance riding.
// 			        </Card.Text>
// 			      </Card.Body>
// 			    </Card>
// 			</Col>
// 		</Row>
// 	)
// }





