import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import givibox from '../images/givibox.webp';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
    const { productId } = useParams();
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [ name, setName ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ price, setPrice ] = useState(0);
    const [ quantity, setQuantity ] = useState(1);
    const [ totalAmount, setTotalAmount ] = useState("");

    const calculateTotalAmount = () => {
        const amount = price * quantity;
        setTotalAmount(amount);
    };

    const checkout = () => {
        fetch(`http://localhost:4000/users/checkout`,
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    productId: productId,
                    quantity: quantity
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if (data && data._id) {
                    Swal.fire({
                        title: "Successful",
                        icon: 'success',
                        text: `You have successfully bought ${ name }`
                    });
                    navigate("/products");
                } else {
                    Swal.fire({
                        title: 'Checkout Failed',
                        icon: 'error',
                        text: 'Login to user account',
                      });
                }
                
            })
            .catch(error => {
                console.error(error);
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            });
    };

    useEffect(() => {
        fetch(`http://localhost:4000/products/${productId}`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setName(data.name);
                setDescription(data.description);
                setPrice(data.price);
            })
            .catch(error => {
                console.error(error);
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });
            });
    }, [ productId ]);

    useEffect(() => {
        calculateTotalAmount();
    }, [ quantity ]);

	return (
		<Container className="mt-5 pt-5">
			<Row>
				<Col lg={{ span: 5, offset: 3}}>
					<Card className="my-3">
					    <Card.Body className="d-flex flex-column align-items-center">
                            <Card.Img className='w-75' variant="top" src={givibox} />
					        <Card.Title>{ name }</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{ description }</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php { price }</Card.Text>
							<Form.Group className="mb-3" controlId="uproductQuantity">
								<Form.Label>Quantity</Form.Label>
								<Form.Control 
									type="number"
									placeholder="Enter quantity"
									value={ quantity }
									onChange={e => setQuantity(e.target.value)}
									required/>
							</Form.Group>
							<Card.Subtitle>Total Price:</Card.Subtitle>
                  			<Card.Text>Php { totalAmount }</Card.Text>
					        { user.id !== null ? 
					        		<Button className='w-100' variant="primary"  onClick={() => checkout(productId)}>Checkout</Button>
					        	: 
					        		<Link className="btn btn-danger btn-block w-100" to="/login">Checkout</Link>
					        }
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
























// import { useState, useEffect, useContext } from 'react';
// import { Container, Card, Button, Row, Col } from 'react-bootstrap';
// import { useParams, useNavigate, Link } from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

// export default function ProductView() {

// 	// The "useParams" hook allows us to retrieve the courseId passed via the URL
// 	const { productId } = useParams();

// 	const { user } = useContext(UserContext);

// 	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
// 	//an object with methods to redirect the user
// 	const navigate = useNavigate();

// 	const [ name, setName ] = useState("");
// 	const [ description, setDescription ] = useState("");
// 	const [ price , setPrice ] = useState(0);

// 	const enroll = (productId) => {
// 		fetch(`http://localhost:4000/users/checkout`,
// 		{
// 			method: 'POST',
// 			headers: {
// 				"Content-Type": "application/json",
// 				"Authorization": `Bearer ${localStorage.getItem('token')}`
// 			},
// 			body: JSON.stringify({
// 				productId: productId
// 			})
// 		})
// 		.then(res => res.json())
// 		.then(data => {

// 			if (data === true) {

// 				Swal.fire({
// 					title: "Successfully enrolled",
// 					icon: 'success',
// 					text: "You have successfully enrolled for this course."
// 				});

// 				// The navigate hook will allow us to navigate and redirect the user back to the courses page programmatically instead of using a component.
// 				navigate("/courses");

// 			} else {

// 				Swal.fire({
// 					title: "Something went wrong",
// 					icon: "error",
// 					text: "Please try again."
// 				});
// 			}
// 		});
// 	}

// 	useEffect(() => {

// 		fetch(`http://localhost:4000/products/${productId}`)
// 		.then(res => res.json())
// 		.then(data => {

// 			console.log(data);

// 			setName(data.name);
// 			setDescription(data.description);
// 			setPrice(data.price);
// 		})

// 	}, [ productId ]);

// 	return (

// 		<Container className="mt-5">
// 			<Row>
// 				<Col lg={{ span: 5, offset: 3}}>
// 					<Card className="my-3">
// 					    <Card.Body>
// 					        <Card.Title>{ name }</Card.Title>
// 					        <Card.Subtitle>Description:</Card.Subtitle>
// 					        <Card.Text>{ description }</Card.Text>
// 					        <Card.Subtitle>Price:</Card.Subtitle>
// 					        <Card.Text>Php { price }</Card.Text>
// 					        { user.id !== null ? 
// 					        		<Button variant="primary"  onClick={() => enroll(productId)}>Checkout</Button>
// 					        	: 
// 					        		<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
// 					        }
// 					    </Card.Body>
// 					</Card>
// 				</Col>
// 			</Row>
// 		</Container>
// 	)

// }