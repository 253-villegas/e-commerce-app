import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
	    title: "Motorcycle Parts and Accessories",
	    content: "Wholesale and Retail",
	    destination: "/products",
	    label: "Browse Products"
	}

	return (
		<>
			<Banner data={ data } />
			<Highlights />
			{/* <footer className='fixed-bottom text-center bg-success'>All rights reserved by KMT Garage 2023</footer> */}
		</>
	)
}