import { useEffect, useState } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products() {

  const [ products, setProducts ] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/`)
    .then(res => res.json())
    .then(data => setProducts(data))
  }, []);

  return (
    <Container>
    <h3 className='pt-5 mt-5 text-center'>Our Products</h3>
      <Row className="productCard justify-content-center mt-5 container-fluid ">
      { products.map(product => (
        
        <Col className='my-3' xs={12} md={6} lg={4} key={product._id}>
          <ProductCard product={product} />
        </Col>
      ))}
    </Row>
    </Container>
    
  )
}
