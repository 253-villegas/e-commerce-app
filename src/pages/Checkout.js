import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CourseView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();
    console.log(productId)

	const { user } = useContext(UserContext);

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a product
	//an object with methods to redirect the user
	const navigate = useNavigate();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price , setPrice ] = useState(0);

	const checkout = (name, quantity) => {
		fetch(`http://localhost:4000/users/checkout`,
		{
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Successful",
					icon: 'success',
					text: "You have successfully bought this product."
				});

				// The navigate hook will allow us to navigate and redirect the user back to the courses page programmatically instead of using a component.
				navigate("/products");

			} else {
				console.log(data)
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {

		console.log(productId);

		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [ productId ]);

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 5, offset: 3}}>
					<Card className="my-3">
					    <Card.Body>
					        <Card.Title>{ name }</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{ description }</Card.Text>
					        <Card.Text>Php { price }</Card.Text>
					        { user.id !== null ? 
					        		<Button className='w-100' variant="primary"  onClick={() => checkout(name)}>Checkout</Button>
					        	: 
					        		<Link className="btn btn-danger " to="/login">Log in to Enroll</Link>
					        }
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)

}