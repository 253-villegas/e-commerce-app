import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { Table, Container, Row, Col } from 'react-bootstrap';

export default function Header() {
  return (
    <Container className="my-5">
      <Row className='justify-content-center align-items-center'>
        <Col>
            <h3 className='text-center pt-5 mb-3 justify-content-center align-items-center'>Admin Dashboard</h3>
            <Table  >
                
                <tbody>
                    <tr className='w-100'>
                      <td>
                          <Link className="btn btn-success btn-block w-100" to="/admin/addProduct">Create Product</Link>
                      </td>
                      <td>
                          <Link className="btn btn-success btn-block w-100" to="/admin/allProducts">All Products</Link>
                      </td>
                      <td>
                          <Link className="btn btn-success btn-block w-100" to="/admin/allOrders">All Orders</Link>
                      </td>
                      <td>
                          <Link className="btn btn-success btn-block w-100" to="/admin/allUsers">All Users</Link>
                      </td>
                      <td>
                          <Link className="btn btn-success btn-block w-100" to="/admin/userDetails">User Details</Link>
                      </td>
                    </tr>
                </tbody>
            </Table>
          
        </Col>
      </Row>
    </Container>

  );
}