import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Header from '../admin/Header';
import { Link } from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';



export default function UserDetails() {
  const [products, setUsers] = useState(null);

  useEffect(() => {
    fetch('http://localhost:4000/products/all', {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => setUsers(data))
      .catch(err => console.log(err));
  }, []);

  

  return (
    <Container>
        <Header />
        <Row  className="mt-5 align-items-center  justify-content-center">
            <Col className='bg-light rounded-3' >
                {products ?
                    <div className="card">
                        <h5 className="card-header bg-success text-center">All Products</h5>
                        <ul className="list-group list-group-flush">
                          {products.map(product => (
                            <li className="list-group-item" key={product._id}>
                              Product ID: { product._id}<br />
                              Product Name: { product.name}<Link className="btn float-end" to={ `/admin/updateProduct/${ product._id}`}><FaEdit size='1.5rem' />
                              </Link><br />
                              Description: { product.description}<br />
                              Price: { product.price}<br />
							  Status: { product.isActive ? 'Active' : 'Archive'}
                              
                            </li>
                          ))}
                          
                        </ul>
                    </div>
                    :
                    <p>Loading product details...</p>
                }
            </Col>
        </Row>
    </Container>

    
  

    );
}

// import { useEffect, useState } from 'react';
// import AdminProductCard from '../admin/AdminProductCard';
// import { Form, Button, Row, Col, Container } from 'react-bootstrap';
// import Header from '../admin/Header';

// export default function AllProducts(){

// 	const [ product, setProducts ] = useState([]);

// 	useEffect(() => {

// 		fetch(`http://localhost:4000/products/all`)
// 		.then(res => res.json())
// 		.then(data => {

// 			setProducts(data.map(product => {

// 				return (
// 					<AdminProductCard key={ product._id } product={product} />
					
// 				);
// 			}));
// 		})
// 	}, []);

// 	return (
// 		<Container>
// 			<Header />
// 			<Row>
// 				<Col>
// 				{ product }
// 				</Col>
// 			</Row>
// 		</Container>
		
// 	)
// }

