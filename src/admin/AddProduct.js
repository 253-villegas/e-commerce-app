
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col, Table } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Header from '../admin/Header';

import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function AddProduct() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!user.isAdmin) {
      navigate("/");
    }
  }, []);

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [isActive, setIsActive] = useState('');

  const addProduct = async (event) => {
    event.preventDefault();

    const response = await fetch('http://localhost:4000/products/addProduct', {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price
      })
    });

    const data = await response.json();

    setName('');
    setDescription('');
    setPrice('');

    Swal.fire({
      title: "Product was added successfully",
      icon: "success",
      text: "Add more product!"
    });

    navigate("/admin/addProduct");
  };

  useEffect(() => {
    if (name !== '' && description !== '' && price !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  return (
    
    <Container fluid className='' bg="light">
      <Header />

      <Row className="align-items-center  justify-content-center">
        <Col className='bg-light' xs={12} md={8} lg={4}>
          <h3 className='text-center my-5'>Create Product</h3>
          <Form onSubmit={addProduct}>
            <Table>
              <tbody>
                <tr>
                  <td>Name</td>
                  <td>
                    <Form.Control type="text" placeholder="Enter product name" value={name} onChange={e => setName(e.target.value)} required />
                  </td>
                </tr>
                <tr>
                  <td>Description</td>
                  <td>
                    <Form.Control type="text" placeholder="Enter product description" value={description} onChange={e => setDescription(e.target.value)} required />
                  </td>
                </tr>
                <tr>
                  <td>Price</td>
                  <td>
                    <Form.Control type="number" placeholder="Enter product price" value={price} onChange={e => setPrice(e.target.value)} required />
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    {isActive ?
                      <Button className='w-100 mt-3' variant="primary" type="submit" id="submitBtn">
                        Create product
                      </Button>
                      :
                      <Button className='w-100 mt-3' variant="success" type="submit" id="submitBtn" disabled>
                        Create product
                      </Button>
                    }
                  </td>
                </tr>
              </tbody>
            </Table>
          </Form>
        </Col>
      </Row>
    </Container>
  )
}
