// import { useState, useEffect, useContext } from 'react';
// import { Container, Table, Button } from 'react-bootstrap';
// import { useParams, useNavigate } from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

// export default function UpdateProduct() {
//   const { productId } = useParams();
//   const { user } = useContext(UserContext);
//   const navigate = useNavigate();

//   const [name, setName] = useState('');
//   const [description, setDescription] = useState('');
//   const [price, setPrice] = useState(0);
//   const [isActive, setIsActive] = useState(false);

//   useEffect(() => {
//     fetch(`http://localhost:4000/products/${productId}`)
//       .then((res) => res.json())
//       .then((data) => {
//         setName(data.name);
//         setDescription(data.description);
//         setPrice(data.price);
//         setIsActive(data.isActive);
//       })
//       .catch((error) => {
//         console.error(error);
//         Swal.fire({
//           title: 'Something went wrong',
//           icon: 'error',
//           text: 'Please try again.',
//         });
//       });
//   }, [productId]);

//   const handleSubmit = (e) => {
//     e.preventDefault();
//     const data = {
//       name,
//       description,
//       price,
//       isActive,
//     };
//     fetch(`http://localhost:4000/products/${productId}`, {
//       method: 'PATCH',
//       headers: {
//         'Content-Type': 'application/json',
//         Authorization: `Bearer ${localStorage.getItem('token')}`,
//       },
//       body: JSON.stringify(data),
//     })
//       .then((res) => res.json())
//       .then((data) => {
//         console.log(data);
//         Swal.fire({
//           title: 'Successful',
//           icon: 'success',
//           text: 'Product details updated successfully!',
//         });
//         navigate('/products');
//       })
//       .catch((error) => {
//         console.error(error);
//         Swal.fire({
//           title: 'Something went wrong',
//           icon: 'error',
//           text: 'Please try again.',
//         });
//       });
//   };

//   return (
//     <Container className="mt-5">
//       <Table striped bordered hover className='text-center'>
//         <thead>
//           <tr className='text-center'>
//             <th >Field</th>
//             <th>Value</th>
//           </tr>
//         </thead>
//         <tbody>
//           <tr>
//             <td>Name</td>
//             <td>
//               <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
//             </td>
//           </tr>
//           <tr>
//             <td>Description</td>
//             <td>
//               <textarea rows={3} value={description} onChange={(e) => setDescription(e.target.value)} />
//             </td>
//           </tr>
//           <tr>
//             <td>Price</td>
//             <td>
//               <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
//             </td>
//           </tr>
//           <tr>
//             <td>Active</td>
//             <td>
//               <input type="checkbox" checked={isActive} onChange={(e) => setIsActive(e.target.checked)} />
//             </td>
//           </tr>
//         </tbody>
//       </Table>
//       <Button variant="primary" type="submit" onClick={handleSubmit}>
//         Save
//       </Button>
//     </Container>
//   );
// }
