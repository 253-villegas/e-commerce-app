import { Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';

export default function AdminProductCard({ product }) {
  const { _id, name, description, price, isActive } = product;

  return (

      
      <Table bordered hover>
        <thead>
          <tr className='text-center'>
            <th className='w-25 text-center'>Name</th>
            <th className='w-75'>Description</th>
            <th className='w-25'>Price</th>
            <th className='w-25'>Status</th>
            <th className='w-25'>Action</th>
          </tr>
        </thead>
        <tbody >
          <tr >
            <td >{name}</td>
            <td>{description}</td>
            <td className='text-center'>₱{price}</td>
            <td className='text-center'>{isActive ? 'Active' : 'Inactive'}</td>
            <td>
              <div className="justify-content-center align-items-center">
                <Link className="btn mt-2" to={`/admin/updateProducts/${_id}`}><FaEdit size='1.5rem' />
                </Link><br />
              </div>
            </td>
          </tr>
        </tbody>
      </Table>
      
  );
}
