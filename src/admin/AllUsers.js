import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Header from '../admin/Header';
import { Link } from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';



export default function UserDetails() {
  const [users, setUsers] = useState(null);

  useEffect(() => {
    fetch('http://localhost:4000/users/all', {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => setUsers(data))
      .catch(err => console.log(err));
  }, []);

  

  return (
    <Container>
        <Header />
        <Row  className="mt-5 align-items-center  justify-content-center">
            <Col className='bg-light rounded-3'>
                {users ?
                    <div className="card">
                        <h5 className="card-header bg-success text-center">All Details</h5>
                        <ul className="list-group list-group-flush">
                          {users.map(user => (
                            <li className="list-group-item" key={user._id}>
                              User ID: {user._id}<br />
                              Full Name: {user.firstName} {user.lastName}<Link className="btn float-end" to={ `/admin/updateUser`}><FaEdit size='1.5rem' />
                              </Link><br />
                              Email: {user.email}<br />
                              Mobile No: {user.mobileNo}
                              
                            </li>
                          ))}
                          
                        </ul>
                    </div>
                    :
                    <p>Loading user details...</p>
                }
            </Col>
        </Row>
    </Container>

    
  

    );
}
